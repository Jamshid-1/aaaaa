package uz.pdp.appbookcrud.service;

import uz.pdp.appbookcrud.entity.Genre;
import uz.pdp.appbookcrud.payload.GenreDTO;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

public class GenreServiceImpl implements GenreService {

    private static GenreServiceImpl instance;

    public List<Genre> genres = Collections.synchronizedList(new ArrayList<>());

    private static Lock lock = new ReentrantLock();

    private GenreServiceImpl() {}

    public static GenreServiceImpl getInstance() {

        if (Objects.isNull(instance)) {
            lock.lock();
            if (Objects.isNull(instance))
                instance = new GenreServiceImpl();
            lock.unlock();
        }

        return instance;
    }

    @Override
    public List<GenreDTO> all() {
        List<GenreDTO> genreDTOList = genres
                .stream()
                .map(genre -> new GenreDTO(genre.getId(), genre.getName()))
                .collect(Collectors.toList());
        return genreDTOList;
    }

    @Override
    public GenreDTO add(GenreDTO genreDTO) {

        int id = genres.size() + 1;

        Genre genre = new Genre(
                id,
                genreDTO.getName()
        );

        genres.add(genre);

        genreDTO.setId(id);

        return genreDTO;
    }

    @Override
    public GenreDTO edit(Integer id, GenreDTO genreDTO) {

        Optional<Genre> optionalGenre = genres
                .stream()
                .filter(genre -> Objects.equals(genre.getId(), id))
                .findFirst();

        Genre genre = optionalGenre.orElseThrow(() -> new RuntimeException("Genre not found with id: " + id));

        genre.setName(genreDTO.getName());
        genreDTO.setId(genre.getId());

        return genreDTO;
    }

    @Override
    public String delete(Integer id) {
        return null;
    }

    @Override
    public Genre getByIdOrElseThrow(Integer id){
        Genre genre = genres
                .stream()
                .filter(oneGenre -> Objects.equals(oneGenre.getId(), id))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Genre not found with id : " + id));
        return genre;
    }
}
