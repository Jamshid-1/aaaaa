package uz.pdp.appbookcrud.service;

import uz.pdp.appbookcrud.payload.BookDTO;

import java.util.List;

public interface BookService {

    //1
    List<BookDTO> getByGenre(Integer genreId);

    //2
    BookDTO getById(Integer id);

    //3
    BookDTO add(BookDTO bookDTO);

    //4
    BookDTO edit(Integer id, BookDTO bookDTO);

    //5
    boolean delete(Integer id);

    //6
    String read(Integer id);

    //7
    boolean serialize();

    //8
    boolean deserialize();
}
