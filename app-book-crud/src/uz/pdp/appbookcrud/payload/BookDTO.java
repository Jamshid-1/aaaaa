package uz.pdp.appbookcrud.payload;

import java.time.LocalDate;

public class BookDTO {

    private Integer id;

    private String name;

    private String author;

    private LocalDate writtenDate;

    private String filePath;

    private Integer genreId;

    private Integer pageCount;

    public BookDTO(Integer id, String name, String author, LocalDate writtenDate, String filePath, Integer genreId, Integer pageCount) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.writtenDate = writtenDate;
        this.filePath = filePath;
        this.genreId = genreId;
        this.pageCount = pageCount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public LocalDate getWrittenDate() {
        return writtenDate;
    }

    public void setWrittenDate(LocalDate writtenDate) {
        this.writtenDate = writtenDate;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }


    public Integer getGenreId() {
        return genreId;
    }

    public void setGenreId(Integer genreId) {
        this.genreId = genreId;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }

    @Override
    public String toString() {
        return "BookDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", writtenDate=" + writtenDate +
                ", filePath='" + filePath + '\'' +
                ", genreId=" + genreId +
                ", pageCount=" + pageCount +
                '}';
    }
}
