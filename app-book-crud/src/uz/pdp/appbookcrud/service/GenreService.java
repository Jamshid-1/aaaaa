package uz.pdp.appbookcrud.service;

import uz.pdp.appbookcrud.entity.Genre;
import uz.pdp.appbookcrud.payload.GenreDTO;

import java.util.List;

public interface GenreService {

    List<GenreDTO> all();

    GenreDTO add(GenreDTO genreDTO);

    GenreDTO edit(Integer id, GenreDTO genreDTO);

    String delete(Integer id);

    Genre getByIdOrElseThrow(Integer id);
}
