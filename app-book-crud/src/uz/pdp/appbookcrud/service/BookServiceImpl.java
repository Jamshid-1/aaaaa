package uz.pdp.appbookcrud.service;

import uz.pdp.appbookcrud.entity.Book;
import uz.pdp.appbookcrud.entity.Genre;
import uz.pdp.appbookcrud.payload.BookDTO;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;
import java.util.logging.*;

public class BookServiceImpl implements BookService {
    private static BookServiceImpl instance;

    public List<Book> books = Collections.synchronizedList(new ArrayList<>());

    private static Lock lock = new ReentrantLock();

    private static Logger logger = Logger.getLogger("Bookservice");

    private BookServiceImpl() {
    }

    public static BookServiceImpl getInstance() {

        if (Objects.isNull(instance)) {
            lock.lock();
            if (Objects.isNull(instance)) {
                instance = new BookServiceImpl();
            }
            lock.unlock();
        }

        return instance;
    }

    @Override
    public List<BookDTO> getByGenre(Integer genreId) {

        logger.log(Level.INFO,"get books by genre param ->"+genreId);

        List<BookDTO> bookDTOList = books
                .stream()
                .filter(book -> book.getGenre().getId().equals(genreId))
                .map(book -> toDTO(book))
                .collect(Collectors.toList());

//        List<Book> filteredBooks = books
//                .stream()
//                .filter(book -> book.getGenre().getId().equals(genreId))
//                .collect(Collectors.toList());
//
//        List<BookDTO> bookDTOList = filteredBooks
//                .stream()
//                .map(book -> toDTO(book))
//                .collect(Collectors.toList());

        return bookDTOList;
    }

    @Override
    public BookDTO getById(Integer id) {

        Book book = getBookByIdOrElseThrow(id);

        BookDTO bookDTO = toDTO(book);
        return bookDTO;
    }

    @Override
    public BookDTO add(BookDTO bookDTO) {

        int id = books.size() + 1;

        List<Genre> genres = GenreServiceImpl.getInstance().genres;

        GenreService genreService = GenreServiceImpl.getInstance();

        Genre genre = genreService.getByIdOrElseThrow(bookDTO.getGenreId());

        Book book = new Book(
                id,
                bookDTO.getName(),
                bookDTO.getAuthor(),
                bookDTO.getWrittenDate(),
                bookDTO.getFilePath(),
                bookDTO.getPageCount(),
                genre
        );

        books.add(book);

        return toDTO(book);
    }

    @Override
    public BookDTO edit(Integer id, BookDTO bookDTO) {

        Book book = getBookByIdOrElseThrow(id);

        book.setName(bookDTO.getName());
        book.setAuthor(bookDTO.getAuthor());
        book.setFilePath(bookDTO.getFilePath());
        book.setPageCount(bookDTO.getPageCount());
        book.setWrittenDate(bookDTO.getWrittenDate());

        GenreService genreService = GenreServiceImpl.getInstance();
        Genre genre = genreService.getByIdOrElseThrow(bookDTO.getGenreId());
        book.setGenre(genre);

        BookDTO bookDTO1 = toDTO(book);
        return bookDTO1;
    }

    @Override
    public boolean delete(Integer id) {

        try {

            Book book = getBookByIdOrElseThrow(id);

            books.remove(book);

            return true;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public String read(Integer id) {

        Book book = getBookByIdOrElseThrow(id);

        Path path = Paths.get(book.getFilePath());

        return readFileViaFileInputStream(path);
    }


    @Override
    public boolean serialize() {

        try(ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("db/books.txt"))) {

            objectOutputStream.writeObject(books);
            return true;

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean deserialize() {

        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("db/books.txt"))){

            books = (List<Book>)objectInputStream.readObject();

            return true;
        }catch (IOException e){
            e.printStackTrace();
            return false;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public BookDTO toDTO(Book book) {
        return new BookDTO(
                book.getId(),
                book.getName(),
                book.getAuthor(),
                book.getWrittenDate(),
                book.getFilePath(),
                book.getGenre().getId(),
                book.getPageCount()
        );
    }

    private Book getBookByIdOrElseThrow(Integer id) {
        Optional<Book> optionalBook = books
                .stream()
                .filter(book -> book.getId().equals(id))
                .findFirst();

        Book book = optionalBook.orElseThrow(() -> new RuntimeException("Book not found with id : " + id));
        return book;
    }

    private String readFileViaFileReader(Path path) {
        try(Reader reader = new FileReader(path.toFile())) {

            StringBuilder stringBuilder = new StringBuilder();

            int symbolNumber = reader.read();
            while (symbolNumber != -1){
                stringBuilder.append((char) symbolNumber);
                symbolNumber = reader.read();
            }

            return stringBuilder.toString();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String readFileViaFileInputStream(Path path) {
        try(InputStream inputStream = new FileInputStream(path.toFile())) {

            byte[] bytes = inputStream.readAllBytes();

            return new String(bytes);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


}
