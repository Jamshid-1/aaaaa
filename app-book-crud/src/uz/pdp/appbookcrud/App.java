package uz.pdp.appbookcrud;

import uz.pdp.appbookcrud.payload.BookDTO;
import uz.pdp.appbookcrud.payload.GenreDTO;
import uz.pdp.appbookcrud.service.BookService;
import uz.pdp.appbookcrud.service.BookServiceImpl;
import uz.pdp.appbookcrud.service.GenreService;
import uz.pdp.appbookcrud.service.GenreServiceImpl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class App {

    //url ->

    public static void main(String[] args) {

        BookServiceImpl bookService = BookServiceImpl.getInstance();
        bookService.deserialize();

//        List<GenreDTO> genreDTOList = generateGenres();
//
//        System.out.println(genreDTOList);
//
//        List<BookDTO> bookDTOList = generateBook(genreDTOList);
//
//        System.out.println(bookDTOList);
//
//        BookService bookService = BookServiceImpl.getInstance();
//
//        Random random = new Random();
//        int index = random.nextInt(bookDTOList.size());
//        BookDTO bookDTO = bookDTOList.get(index);
//
//        String read = bookService.read(bookDTO.getId());
//        System.out.println("read -> " + read);
//
//        int genreIndex = random.nextInt(genreDTOList.size());
//        GenreDTO genreDTO = genreDTOList.get(genreIndex);
//        List<BookDTO> booksByGenre = bookService.getByGenre(genreDTO.getId());
//        System.out.println("books by genre -> " + booksByGenre);
//
//        boolean serialize = bookService.serialize();
//        System.out.println(serialize);
//
//        boolean deserialize = bookService.deserialize();
//        System.out.println(deserialize);
    }

    private static List<BookDTO> generateBook(List<GenreDTO> genreDTOList) {
        BookService bookService = BookServiceImpl.getInstance();
        List<BookDTO> bookDTOList = new ArrayList<>();

        Random random = new Random();

        for (int i = 0; i < 400; i++) {
            int genreIndex = random.nextInt(genreDTOList.size());
            GenreDTO genreDTO = genreDTOList.get(genreIndex);
            BookDTO bookDTO = bookService.add(new BookDTO(
                    null,
                    "Book name -> " + i,
                    "Author -> " + i,
                    LocalDate.of(1940, 8, 5),
                    "booksFiles/bookContent.txt",
                    genreDTO.getId(),
                    i + 400
            ));
            bookDTOList.add(bookDTO);
        }
        return bookDTOList;
    }

    private static List<GenreDTO> generateGenres() {
        GenreService genreService = GenreServiceImpl.getInstance();

        List<GenreDTO> genreDTOList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            GenreDTO genreDTO = genreService.add(new GenreDTO(null, "Genre -> " + i));
            genreDTOList.add(genreDTO);
        }
        return genreDTOList;
    }

}
